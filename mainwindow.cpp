#include "configuration_dialog.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"

#include <QDialog>
MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    this->setFixedSize(432,600);
    ui->setupUi(this);
    connect( ui->startnewgame  , &QAction::triggered, this, &MainWindow::startnewgame );
    connect( ui->gameboard     , &tictactoewidget::gameover, this, &MainWindow::handlegameover);
    connect(ui->gameboard,  &tictactoewidget::currentplayerchanged, this, &MainWindow::updatenamelabels);
    connect(ui->quit, &QAction::triggered, qApp, &QApplication::quit);
    connect(ui->restartgame, &QAction::triggered, this, &MainWindow::restartnewgame);
    connect(ui->gameboard, &tictactoewidget::invalid_move, this, &MainWindow::handle_invalid_move);
    ui->restartgame->setEnabled(gamestarted);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::handlegameover(tictactoewidget::player winner)
{
    QString message,winnername;
    if(winner == tictactoewidget::player::draw){
        message = tr("game ended in a draw.");
    }
    else if(winner == tictactoewidget::player::player1){
        winnername= ui->player1name->text();
    }
    else {
        winnername = ui->player2name->text();
    }
    if(message.isNull()){
        message = winnername+" wins!!";
    }
    QMessageBox::information(this,tr("info"), message);
    ui->gameboard->setcurrentplayer(tictactoewidget::player::invalid);
}

void MainWindow::startnewgame()
{
    configuration_dialog dialog(this);
    if (dialog.exec()==QDialog::Rejected ){
        return ;// user clicked cancel button :yayy:
    }
    ui->player1name->setText(dialog.Player1name());
    ui->player2name->setText(dialog.Player2name());
    ui->gameboard->initnewgame();
    if(gamestarted == false){
        gamestarted = true;
        ui->restartgame->setEnabled(gamestarted);
    }
}

void MainWindow::updatenamelabels()
{
    setlabelbold(ui->player1name,(ui->gameboard->currentplayer() == tictactoewidget::player::player1));
    setlabelbold(ui->player2name,(ui->gameboard->currentplayer() == tictactoewidget::player::player2));
}

void MainWindow::restartnewgame()
{
    ui->gameboard->initnewgame();
}

void MainWindow::handle_invalid_move()
{
    ui->statusbar->showMessage("Please start a new game.");
        return ;
}

void MainWindow::setlabelbold(QLabel *label, bool isbold)
{
    QFont f = label->font();
    f.setBold(isbold);
    label->setFont(f);
    return ;
}




