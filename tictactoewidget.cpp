#include "tictactoewidget.h"
#include "qdebug.h"

#include <QGridLayout>
#include <QSignalMapper>

tictactoewidget::tictactoewidget(QWidget *parent)
    : QWidget{parent}
{
    m_currentplayer = player::invalid;
    QGridLayout *gridlayout = new QGridLayout(this);
    QSignalMapper *mapper = new  QSignalMapper(this);
    for (int row = 0; row < 3; ++row) {
        for (int coloumn = 0; coloumn < 3; ++coloumn) {
            QPushButton *button = new QPushButton(" ");
            button->setSizePolicy(QSizePolicy::Preferred,QSizePolicy::Preferred);
            gridlayout->addWidget(button, row, coloumn);
            m_board.push_back(button);
            mapper->setMapping(button, m_board.size()-1);
            connect(button, SIGNAL(clicked()), mapper, SLOT(map()) );
        }
    }
    connect(mapper, SIGNAL(mapped(int)), this,   SLOT(handlebuttonclick(int)));
}

void tictactoewidget::setcurrentplayer(player p)
{
    if(m_currentplayer == p)
    {
        return ;
    }
    m_currentplayer = p;
    emit currentplayerchanged(p);
}

void tictactoewidget::initnewgame()
{
    for(auto *button : m_board) {
        button->setText(" ");
    }
    setcurrentplayer(player::player1);
}


void tictactoewidget::handleButtonClick(int index)
{
    if (m_currentplayer == player::invalid) {
        emit invalid_move();
        return; // game is not started
    }
    if((index) < 0 || (index >= (int)(m_board.size()))) {
        return; // out of bounds check
    }
    QPushButton *button = m_board[index];
    if(button->text() != " ") return; // invalid move
    button->setText(currentplayer() == player::player1 ? "X" : "O");
    player winner = checkWinCondition();
    if(winner == player::invalid) {
        setcurrentplayer(currentplayer() == player::player1 ? player::player2 : player::player1);
        return;
    } else {
        qDebug() << "gameover" << winner;
        emit gameover(winner);
    }
}

tictactoewidget::player tictactoewidget::checkWinCondition() const
{
    player result = player::invalid;
    // check horizontals
    for(int row = 0; row < 3; ++row) {
        result = checkWinConditionForLine(row * 3,
                                          row * 3 + 1,
                                          row * 3 + 2);
        if (result != player::invalid) {
            return result;
        }
    }
    // check verticals
    for(int column = 0; column < 3; ++column) {
         result = checkWinConditionForLine(column,
                                           3 + column,
                                           6 + column);
        if (result != player::invalid) {
            return result;
        }
    }
    // check diagonals
    result = checkWinConditionForLine(0, 4, 8);
    if (result != player::invalid) {
        return result;
    }
    result = checkWinConditionForLine(2, 4, 6);
    if (result != player::invalid) {
        return result;
    }
    // check if there are unoccupied fields left
    for(QPushButton *button: m_board) {
        if(button->text() == " ") {
            return player::invalid;
        }
    }
    return player::draw;
}

void tictactoewidget::handlebuttonclick(int index)
{
    handleButtonClick(index);
}

