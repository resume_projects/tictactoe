#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <tictactoewidget.h>
#include <QMessageBox>
#include <QLabel>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class QPushButton ;
class MainWindow : public QMainWindow
{
    Q_OBJECT
    bool gamestarted = false;
public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();



public slots:
    void handlegameover(tictactoewidget::player winner);
    void startnewgame();
    void updatenamelabels();
    void restartnewgame();
    void handle_invalid_move();


private:
    Ui::MainWindow *ui;
    void setlabelbold(QLabel* label, bool isbold);
};
#endif // MAINWINDOW_H
