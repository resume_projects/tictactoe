#ifndef CONFIGURATION_DIALOG_H
#define CONFIGURATION_DIALOG_H

#include <QDialog>

namespace Ui {
class configuration_dialog;
}

class configuration_dialog : public QDialog
{
    Q_OBJECT

public:
    explicit configuration_dialog(QWidget *parent = nullptr);
    ~configuration_dialog();
    void setPlayer1name(const QString &p1name);
    void setPlayer2name(const QString &p2name);
    QString Player1name() const;
    QString Player2name() const;
private:
    Ui::configuration_dialog *ui;
private slots:
    void updateokbuttonstate();
};

#endif // CONFIGURATION_DIALOG_H
