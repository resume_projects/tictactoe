#include "configuration_dialog.h"
#include "ui_configuration_dialog.h"

#include <QPushButton>

configuration_dialog::configuration_dialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::configuration_dialog)
{
    ui->setupUi(this);
    connect(ui->player1name, &QLineEdit::textChanged,this, &configuration_dialog::updateokbuttonstate);
    connect(ui->player2name, &QLineEdit::textChanged,this, &configuration_dialog::updateokbuttonstate);
    updateokbuttonstate();
}

configuration_dialog::~configuration_dialog()
{
    delete ui;
}

void configuration_dialog::setPlayer1name(const QString &p1name)
{
    ui->player1name->setText(p1name);
}

void configuration_dialog::setPlayer2name(const QString &p2name)
{
    ui->player2name->setText(p2name);
}

QString configuration_dialog::Player1name() const
{
    return ui->player1name->text();
}

QString configuration_dialog::Player2name() const
{
    return ui->player2name->text();
}

void configuration_dialog::updateokbuttonstate()
{
    QPushButton* okbutton = ui->buttonBox->button(QDialogButtonBox::Ok);
    okbutton->setEnabled(!ui->player1name->text().isEmpty() and
                         !ui->player2name->text().isEmpty() and
                         ui->player2name->text()!=ui->player1name->text());
}
