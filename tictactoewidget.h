#ifndef TICTACTOEWIDGET_H
#define TICTACTOEWIDGET_H

#include <QPushButton>
#include <QVector>
#include <QWidget>
#include <vector>

class tictactoewidget : public QWidget
{
    Q_OBJECT
public:
    explicit tictactoewidget(QWidget *parent = nullptr);
    enum class player {
        invalid, player1, player2, draw
    };
    Q_ENUM(player);

    player currentplayer() const
    {
        return m_currentplayer;
    }
    void setcurrentplayer(player p);

    void initnewgame();
    void handleButtonClick(int index);
    player m_currentplayer = player::invalid;

public slots:
     void handlebuttonclick(int index);

private:
    std::vector<QPushButton* > m_board;

    tictactoewidget::player checkWinCondition() const;
    tictactoewidget::player checkWinConditionForLine(int index1, int index2, int index3) const {
        QString text1 = m_board[index1]->text();
        if (text1 == " ") {
            return player::invalid;
        }
        QString text2 = m_board[index2]->text();
        QString text3 = m_board[index3]->text();
        if (text1 == text2 && text1 == text3) {
            return text1 == "X" ? player::player1 : player::player2;
        }
        return player::invalid;
    }
signals:
    void invalid_move();
    void currentplayerchanged(tictactoewidget::player);
    void gameover(tictactoewidget::player);
};

#endif // TICTACTOEWIDGET_H
